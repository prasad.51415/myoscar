from oscar.apps.checkout import config


class CheckoutConfig(config.CheckoutConfig):
    name = 'ecommerce\apps\checkout'
